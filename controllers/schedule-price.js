/**
 * @file Scheduller for updating pairs
 */

const fastifyPlugin = require('fastify-plugin')
const { fastifySchedulePlugin } = require('fastify-schedule')
const { SimpleIntervalJob, AsyncTask } = require('toad-scheduler')
const { config, getFromApi } = require('../config/price-api')

// Job settings
const JOB_INTERVAL = 120 // Interval in SECONDS
const JOB_RUN_ON_START = true // Run job on start?
const JOB_DB_COLLECTION = 'prices'

let db = null

/**
 * Make ID for pair
 * @param fsym
 * @param tsym
 * @returns {string}
 */
function makePairId (fsym, tsym) {
  return `${fsym}-${tsym}`
}

/**
 * Process API response and split it to the fsym/tsym pairs.
 * Currently uses only RAW result.
 * @param res API response (assuming it has RAW and DISPLAY objects)
 * @returns {*[]}
 */
function splitByPair (res) {
  if (res.RAW) {
    const RAW = res.RAW
    const tstamp = Date.now()
    const pairs = []

    Object.keys(RAW).forEach(fsym => {
      Object.keys(RAW[fsym]).forEach(tsym => {
        pairs.push({
          _id: makePairId(fsym, tsym),
          updated: tstamp,
          data: RAW[fsym][tsym]
        })
      })
    })

    return pairs
  }
  else {
    console.warn('Wrong API response: no RAW data')
    return []
  }
}

/**
 * Save formatted pairs data to MongoDB
 * @param data
 * @returns {Promise<{}>}
 */
async function saveToDb (data) {
  try {
    const collection = db.collection(JOB_DB_COLLECTION)
    await collection.drop()
    const res = await collection.insertMany(data)
    return res
  }
  catch (error) {
    console.warn('Error in saving to DB', error)
    return {}
  }
}

/**
 * Job logic for price updating
 * @returns {Promise<void>}
 */
async function jobGetPrice () {
  const res = await getFromApi(config.fsyms, config.tsyms)

  if (res.data) {
    const pairs = splitByPair(res.data)
    if (pairs.length) {
      const saved = await saveToDb(pairs)
      if (saved.insertedCount) {
        console.info('Job ran succesfully. Updated pairs: %s', saved.insertedCount)
      }
    }
  }
  else {
    console.warn('Job has errors. Nothing updated.')
  }
}

/**
 * Init Scheduler (Fastify plugin hook)
 * @param fastify
 * @param options
 * @returns {Promise<void>}
 */
async function scheduler (fastify, options) {
  fastify.register(fastifySchedulePlugin)
  db = fastify.mongo.db // Get Mongo connection and bring it to module's scope

  // Prepare task
  const taskId = 'updatePriceTask'
  const task = new AsyncTask(
    taskId,
    jobGetPrice,
    (err) => {
      console.error('Error on adding task %s', taskId, err)
    }
  )

  const job = new SimpleIntervalJob({
    seconds: JOB_INTERVAL,
    runImmediately: JOB_RUN_ON_START
  }, task)

  // Adding task after Fastify is ready
  fastify.ready().then(() => {
    fastify.scheduler.addSimpleIntervalJob(job)
  })
}

module.exports = {
  plugin: fastifyPlugin(scheduler),
  dbCollection: JOB_DB_COLLECTION,
  makePairId
}

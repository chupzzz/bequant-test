/**
 * @file MongoDB settings and connect to Fastify
 */

const fastifyPlugin = require('fastify-plugin')

const HOST = process.env.MONGO_HOST || 'localhost'
const PORT = process.env.MONGO_PORT || 27017
const DB = process.env.MONGO_DB || 'bequant'

async function mongoConnector (fastify, options) {
  fastify.register(require('fastify-mongodb'), {
    url: `mongodb://${HOST}:${PORT}/${DB}`
  })

  fastify.ready().then(async () => {
    // Adding simple document to init collection
    await fastify.mongo.db.collection('prices').insertOne({ init: true })
  })
}

module.exports = fastifyPlugin(mongoConnector)

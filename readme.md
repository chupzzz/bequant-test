# Bequant Price API service
*Test for NodeJS developer by Ruslan Zhizherin [chupzzz@gmail.com]*

## Technical information

Service was developed using **Docker CE v20** and **Docker-Compose 1.29** on Debian 10

### Containers:

- node:14 (Node.js v14)
- mongo:4.4.11-focal (MongoDB v4.4.11)

### .env file

Critical application settings should be stored in the */.env* file. Create yours using following variables:

```
APP_INTERFACE=localhost
APP_PORT=8000

MONGO_HOST=localhost
MONGO_PORT=27017
MONGO_DB=bequant
```

### Ports

Service exposed to 80 port on a host machine. Adjust it in *docker-compose.yml*

## Start the service

Be sure you have *.env* in the project root before starting the service.

Run *docker-compose* as usual:

```
docker-compose up
```

## Application details

Service created using NodeJS and Fastify as a framework.

MongoDB has only one collection to store pairs data. Collection is created automatically upon service start.

As a scheduler used *fastify-schedule* plugin. It runs the update-price job every 120 seconds (configurable in *schedule-price.js*)

List of all fsyms/tsyms pairs is configurable in *./config/price-api.js*. Only listed pairs are available for request in service because they're stored in DB. 

## Available endpoints

### [GET] /

Default route that listing all available endpoints.

### [GET] /price

Returns price data for passed fsym/tsym pairs.

*Using local MongoDB data that refreshed every 120 seconds*

**Parameters:**

- **fsyms** - one or multiple codes divided by comma
- **tsyms** - one or multiple codes divided by comma

**Returns**

JSON list of fsyms/tsyms pairs

**Example**

```bash
curl http://localhost/price?fsyms=BTC,ADA,LINK&tsyms=USD,RUB
```

## TODO

- Make support of any pairs (not only stored in DB)
- Use Redis as caching instead of MongoDB
- Rewrite in TypeScript
- Add tests

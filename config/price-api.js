/**
 * @file Settings and core functions to work with API
 */

const axios = require('axios')

const config = {
  endpoint: 'https://min-api.cryptocompare.com/data/pricemultifull',
  fsyms: ['ETH', 'ADA', 'BTC', 'BNB', 'SOL', 'XRP', 'LUNA', 'DOT', 'DOGE', 'SHIB', 'UNI', 'CRO', 'LINK'],
  tsyms: ['USD', 'EUR', 'RUB', 'BTC', 'ETH', 'USDT']
}

/**
 * Get fsyms/tsyms data from API
 * @param fsyms
 * @param tsyms
 * @returns {Promise<AxiosResponse<any>|{data: null, error, status: boolean}>}
 */
async function getFromApi (fsyms, tsyms) {
  try {
    const res = await axios.get(
      config.endpoint,
      {
        params: {
          fsyms: fsyms.join(','),
          tsyms: tsyms.join(',')
        }
      }
    )

    return res
  }
  catch (error) {
    console.error('Error on API call', error.code)
    return { status: false, data: null, error }
  }
}

module.exports = {
  config, getFromApi
}

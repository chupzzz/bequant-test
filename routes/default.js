/**
 * @file Handler for root route. Provides a list of available endpoints.
 */

async function routes (fastify, options) {
  fastify.get('/', async (request, reply) => {
    return {
      'Available endpoints': {
        '/price': { method: 'GET', parameters: ['fsym', 'tsym'] }
      }
    }
  })
}

module.exports = routes

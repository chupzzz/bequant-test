/**
 * @file Handler for /price route. Returns requested fsyms/tsyms
 */

const { dbCollection, makePairId } = require('../controllers/schedule-price')

async function processRequest (request, collection) {
  const { fsyms, tsyms } = request.query

  // Get pairs names
  const pairs = []
  fsyms.split(',').forEach(fsym => {
    tsyms.split(',').forEach(tsym => {
      pairs.push(makePairId(fsym, tsym))
    })
  })

  // Retrieve pairs data from DB
  const pairsObj = {}
  await collection
    .find({ _id: { $in: pairs } })
    .forEach(pair => { pairsObj[pair._id] = pair.data })

  // Format result data
  const res = {}

  fsyms.split(',').forEach(fsym => {
    let pairData = {}
    tsyms.split(',').forEach(tsym => {
      const pairId = makePairId(fsym, tsym)
      pairData[tsym] = pairsObj[pairId]
    })
    res[fsym] = pairData
  })

  return res
}

// Schema for GET query
const opts = {
  schema: {
    summary: 'get prices',
    querystring: {
      type: 'object',
      required: ['fsyms', 'tsyms'],
      properties: {
        fsyms: { type: 'string' },
        tsyms: { type: 'string' },
      }
    }
  }
}

async function routes (fastify, options) {
  fastify.get('/price', opts, async (request, reply) => {
    const collection = fastify.mongo.db.collection(dbCollection)
    const result = await processRequest(request, collection)

    return result
  })
}

module.exports = routes

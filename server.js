/**
 * @file Server entry-point. Initialize routes and plugins.
 */

// Service config
const PORT = process.env.APP_PORT || 8000
const INTERFACE = process.env.APP_INTERFACE || '127.0.0.1'

// Fastify
const fastify = require('fastify')({ logger: true })

// MongoDB
fastify.register(require('./controllers/init-mongo'))

// Scheduler
const scheduler = require('./controllers/schedule-price').plugin
fastify.register(scheduler)

// Routes
fastify.register(require('./routes/default')) // [/] Default route
fastify.register(require('./routes/price')) // [/price] Price route

// Start server
const start = async () => {
  try {
    await fastify.listen(PORT, INTERFACE)
    console.info(`server listening on ${fastify.server.address().port}`)
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}
start()
